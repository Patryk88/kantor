import React from 'react';

import { Router, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

import { spacings, colors } from './styles/variables';
import history from './helpers/history';
import { logOut } from './services/userService';

import Footer from './components/footer';
import Exchange from './components/exchange';
import Login from './auth/login';
import Register from './auth/register';
import Nav from './components/navigation';
import NotFound from './components/notFound404';

const MainContainer = styled.div`
  padding: ${spacings.xxxl} 0;
  background-color: ${colors.white};
`;

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      loggedIn: !!window.localStorage.getItem('userToken'),
      userName: window.localStorage.getItem('userName')
    };
  }
  onLogOut = () => {
    logOut();
    this.setState({
      loggedIn: false
    });
  }

  onLoggedIn = () => {
    this.setState({
      loggedIn: true,
      userName: window.localStorage.getItem('userName')
    });
  }

  render() {
    const { loggedIn, userName } = this.state;
    return (
      <MainContainer>
        <Nav loggedIn={loggedIn} logOut={this.onLogOut} userName={userName} />
        <Router history={history}>
          <Switch>
            <Route path="/login" render={() =>  <Login loggedIn={this.onLoggedIn} /> } />
            <Route path="/register" component={Register} />
            <Route exact path="/" component={Exchange} />
            <Route component={NotFound} />
          </Switch>
        </Router>
        <Footer />
        <ToastContainer pauseOnHover={false} position="top-center" />
      </MainContainer>
    );
  }
}

export default App;
