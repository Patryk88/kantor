import React from 'react';
import PropTypes from 'prop-types';

import {
  Form,
  InputGroup,
  Label,
  Input,
  Button
} from '../styles/commonStyles';

const propTypes = {
  usd: PropTypes.number,
  eur: PropTypes.number,
  czk: PropTypes.number,
  chf: PropTypes.number,
  gbp: PropTypes.number,
  rub: PropTypes.number,
  pln: PropTypes.number,
  handleChange: PropTypes.func.isRequired
};

const defaultProps = {
  usd: 0,
  eur: 0,
  czk: 0,
  chf: 0,
  gbp: 0,
  rub: 0,
  pln: 0
};

const Wallet = ({ handleCreateWallet, handleChange, usd, eur, chf, czk, rub, gbp, pln }) =>
    <Form onSubmit={handleCreateWallet}>
      <InputGroup>
        <InputGroup>
          <Label htmlFor="pln">PLN</Label>
          <Input
            required
            type="number"
            min={0}
            step={0.01}
            value={pln}
            name="pln"
            onChange={handleChange}
            placeholder="please fill your wallet..."
          />
        </InputGroup>
        <Label htmlFor="usd">USD</Label>
        <Input
          required
          type="number"
          min={0}
          step={0.01}
          value={usd}
          name="usd"
          onChange={handleChange}
          placeholder="please fill this field..."
        />
      </InputGroup>
      <InputGroup>
        <Label htmlFor="eur">EUR</Label>
        <Input
          required
          type="number"
          min={0}
          step={0.01}
          value={eur}
          name="eur"
          onChange={handleChange}
          placeholder="please fill this field..."
        />
      </InputGroup>
      <InputGroup>
        <Label htmlFor="chf">CHF</Label>
        <Input
          required
          type="number"
          min={0}
          step={0.01}
          value={chf}
          name="chf"
          onChange={handleChange}
          placeholder="please fill this field..."
        />
      </InputGroup>
      <InputGroup>
        <Label htmlFor="rub">RUB</Label>
        <Input
          required
          type="number"
          step={0.01}
          min={0}
          value={rub}
          name="rub"
          onChange={handleChange}
          placeholder="please fill this field..."
        />
      </InputGroup>
      <InputGroup>
        <Label htmlFor="czk">CZK</Label>
        <Input
          required
          type="number"
          step={0.01}
          min={0}
          value={czk}
          name="czk"
          onChange={handleChange}
          placeholder="please fill this field..."
        />
      </InputGroup>
      <InputGroup>
        <Label htmlFor="gbp">GBP</Label>
        <Input
          required
          type="number"
          min={0}
          step={0.01}
          value={gbp}
          name="gbp"
          onChange={handleChange}
          placeholder="please fill this field..."
        />
      </InputGroup>
      <Button type="Submit" value="Submit">
        Create Wallet
      </Button>
    </Form>;

Wallet.propTypes = propTypes;
Wallet.defaultProps = defaultProps;
export default Wallet;
