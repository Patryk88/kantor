import React from 'react';
import { Link } from 'react-router-dom';
import history from '../helpers/history';
import { authenticate } from '../services/userService';

import styled from 'styled-components';
import { colors, spacings } from '../styles/variables';

import {
  MainContainer,
  ContainerBox,
  InnerContainer,
  Header,
  Form,
  InputGroup,
  Label,
  Input,
  Button
} from '../styles/commonStyles';

const LinkToRegister = styled(Link)`
    margin-top: ${spacings.l};
    color: ${colors.black};
    font-size:${spacings.m};
    text-align: right;

    :hover {
      color: ${colors.blue};
    }
`;

class Login extends React.Component {
  constructor(){
    super();
    this.state = {
      loginUsername: '',
      loginPassword: '',
    };
    if (window.localStorage.getItem('userToken')) {
      history.push('/');
    }
  }
  handleLogin  = async e => {
    e.preventDefault();
    await authenticate({
      username: this.state.loginUsername,
      password: this.state.loginPassword
    });
    if (window.localStorage.getItem('userToken')) {
      this.props.loggedIn();
    }

  };

  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    this.setState({ [name]: e.target.value });
  };
  render() {
    const { loginUsername, loginPassword } = this.state;
    return (
      <MainContainer>
        <ContainerBox>
          <InnerContainer>
              <Header>Login</Header>
              <Form onSubmit={this.handleLogin}>
                <InputGroup>
                  <Label htmlFor="loginUsername">Username</Label>
                  <Input
                    type="text"
                    value={loginUsername}
                    name="loginUsername"
                    onChange={this.handleChange}
                    placeholder="Username"
                  />
                </InputGroup>
                <InputGroup>
                  <Label htmlFor="loginPassword">Password</Label>
                  <Input
                    type="password"
                    value={loginPassword}
                    name="loginPassword"
                    onChange={this.handleChange}
                    placeholder="Password"
                  />
                </InputGroup>
                <Button type="Submit" value="Submit">
                  Sign In
                </Button>
                <LinkToRegister to="../register"> Create an account</LinkToRegister>
              </Form>
            </InnerContainer>
          </ContainerBox>
        </MainContainer>
    );
  }
}

export default Login;
