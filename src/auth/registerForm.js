import React from 'react';

import {
  Form,
  InputGroup,
  Label,
  Input,
  Button
} from '../styles/commonStyles';


const RegisterForm = ({ registerUsername,registerPassword, handleRegister, handleChange}) => (
  <Form onSubmit={handleRegister}>
    <InputGroup>
      <Label htmlFor="registerUsername">Username</Label>
      <Input
        type="text"
        value={registerUsername}
        name="registerUsername"
        onChange={handleChange}
        placeholder="Username"
      />
    </InputGroup>
    <InputGroup>
      <Label htmlFor="registerPassword">Password</Label>
      <Input
        type="password"
        value={registerPassword}
        name="registerPassword"
        onChange={handleChange}
        placeholder="Password"
      />
    </InputGroup>
    <Button type="Submit" value="Submit">
      Register
    </Button>
  </Form>
);

export default RegisterForm;