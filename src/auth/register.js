import React from 'react';

import Wallet from './wallet';
import RegisterForm from './registerForm';
import history from '../helpers/history';

import styled from 'styled-components';
import { spacings, breakpoints } from '../styles/variables';

import {
  MainContainer,
  ContainerBox,
  InnerContainer,
  Header
} from '../styles/commonStyles';

import { createUser } from '../services/userService';
import { createWallet } from '../services/walletService';

const Steps = styled.h2`
  padding-bottom: 5rem;
  font-size: ${spacings.xxl};
  @media ${breakpoints.mediumAuth} {
    padding-bottom: 10rem;
  }
`;
class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      registerUsername: '',
      registerPassword: '',
      userId: '',
      pln: 0,
      usd: 0,
      eur: 0,
      chf: 0,
      rub: 0,
      czk: 0,
      gbp: 0,
      display: 0
    };
    if (window.localStorage.getItem('userToken')) {
      history.push('/');
    }
  }

  handleRegister = async e => {
    e.preventDefault();
    const response = await createUser({
      username: this.state.registerUsername,
      password: this.state.registerPassword
    });
    if (response.success) {
    this.setState({
      display: 1,
      userId: response.userId
    });
    }
  };

  handleCreateWallet = async e => {
    e.preventDefault();
    await createWallet({
      userId: this.state.userId,
      PLN: this.state.pln,
      USD: this.state.usd,
      EUR: this.state.eur,
      CHF: this.state.chf,
      RUB: this.state.rub,
      CZK: this.state.czk,
      GBP: this.state.gbp,
    });
  };

  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    this.setState({ [name]: e.target.type === 'number' ? parseFloat(e.target.value).toFixed(2) : e.target.value });
  };

  render() {
    const {
      registerUsername,
      registerPassword,
      usd,
      eur,
      czk,
      gbp,
      rub,
      chf,
      pln,
      display
     } = this.state;
    return (
  <MainContainer>
    <Steps>step {display + 1}</Steps>
    <ContainerBox>
      <InnerContainer>
        <Header>Register</Header>
        {display === 0 ?
        <RegisterForm
          handleRegister={this.handleRegister}
          handleChange={this.handleChange}
          registerUsername={registerUsername}
          registerPassword={registerPassword}/>
        :
        <Wallet
          handleCreateWallet={this.handleCreateWallet}
          handleChange={this.handleChange}
          usd={usd}
          chf={chf}
          czk={czk}
          rub={rub}
          gbp={gbp}
          pln={pln}
          eur={eur}/> }
      </InnerContainer>
    </ContainerBox>
  </MainContainer>
    );
  }
}

export default Register;
