import { toast } from 'react-toastify';
import { API_URL } from '../config/';

export const transaction = async (body) => {
	try {
		const response = await fetch(`${API_URL}/transactions/`, {
			method: 'POST',
			body: JSON.stringify(body),
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${window.localStorage.getItem('userToken')}`
			}
		});

		const { message } = await response.json();

		if (response.ok) {
			toast.success('Transaction Successful');
		} else {
      throw new Error(message);
    }
	} catch (error) {
			toast.error(error.message);
	}
};

export const fetchTransactions = async () => {
	try {
		const response = await fetch(`${API_URL}/transactions`, {
			headers: {
				Authorization: `Bearer ${window.localStorage.getItem('userToken')}`
			}
		});

		return await response.json();
	} catch (error) {
		throw new Error(error);
	}
};