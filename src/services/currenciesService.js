export const fetchCurrencies = async () => {
  try {
    const response = await fetch('/currencies');

    return await response.json();
  } catch (error) {
    throw new Error(error);
  }
};
