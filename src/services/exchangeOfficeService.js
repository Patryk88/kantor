import { API_URL } from '../config/';

export const fetchExchangeOffice = async () => {
	try {
		const response = await fetch(`${API_URL}/exchange-office/`, {
			headers: {
				Authorization: `Bearer ${window.localStorage.getItem('userToken')}`
			}
		});

		return await response.json();
	} catch (error) {
		throw new Error(error);
	}
};

export const updateExchangeOffice = async (id, body) => {
	try {
		const response = await fetch(`${API_URL}/exchange-office/${id}`, {
			method: 'PUT',
			body: JSON.stringify(body),
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${window.localStorage.getItem('userToken')}`
      }
		});

		return await response.json();
	} catch (error) {
		throw new Error(error);
	}
};