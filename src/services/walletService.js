import { toast } from 'react-toastify';
import { API_URL } from '../config/';

import history from '../helpers/history';

export const createWallet = async body => {
	try {
		const response = await fetch(`${API_URL}/wallets/`, {
			method: 'POST',
			body: JSON.stringify(body),
			headers: {
				'Content-Type': 'application/json'      }
		});
		history.push('/login');
		toast.success('Successfully Registered');
		return await response.json();
	} catch (error) {
		throw new Error(error);
	}
};

export const fetchWallet = async (id) => {
	try {
		const response = await fetch(`${API_URL}/wallets/${id}`, {
			headers: {
				Authorization: `Bearer ${window.localStorage.getItem('userToken')}`
			}
		});

		return await response.json();
	} catch (error) {
		throw new Error(error);
	}
};

export const updateWallet = async (id, body) => {
	try {
		const response = await fetch(`${API_URL}/wallets/${id}`, {
			method: 'PUT',
			body: JSON.stringify(body),
			headers: {
				'Content-Type': 'application/json',
				authorization: `Bearer ${window.localStorage.getItem('userToken')}`
      }
		});

		return await response.json();
	} catch (error) {
		throw new Error(error);
	}
};
