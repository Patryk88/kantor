import { API_URL } from '../config/';
import { toast } from 'react-toastify';
import history from '../helpers/history';

export const createUser = async body => {
  try {
    const response = await fetch(`${API_URL}/users/register`, {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      }
    });
    const resp = await response.json();
    if (response.ok) {
      return { success: true, userId: resp.userId };
    } else {
      throw new Error('User is Already taken');
    }
  } catch (error) {
    toast.error(error.message);
    return { success: false };
  }
};

export const authenticate = async body => {
  try {
    const response = await fetch(`${API_URL}/users/authenticate`, {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const {_id, token, username } = await response.json();
    if (response.ok) {
      window.localStorage.setItem('userId', _id);
      window.localStorage.setItem('userToken', token);
      window.localStorage.setItem('userName', username);
      toast.success('Successfully logged in');
      history.push('/');
    } else {
      throw new Error('Wrong username or password');
    }
  } catch (error) {
    toast.error(error.message);
  }
};

export const logOut = () => {
  history.push('/login');
  window.localStorage.removeItem('userId');
  window.localStorage.removeItem('userToken');
  window.localStorage.removeItem('userName');
};
