import React from 'react';
import styled from 'styled-components';
import { spacings, colors } from '../styles/variables';

const MainContainer = styled.section`
  position: fixed;
  bottom: 0;
  width: 100%;
  padding: ${spacings.m};
  background-color: #2c3e50;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Copyrights = styled.h2`
  font-weight: 400;
  padding: 1rem;
  color: ${colors.white};
`;
const Links = styled.a`
  color: ${colors.green};
`;

const Footer = () => {
  return (
    <MainContainer>
      <Copyrights>Copyrights 2018</Copyrights>
      <Copyrights>
        Icon made by{' '}
        <Links href="https://www.freepik.com" title="Freepik">
          Freepik
        </Links>
      </Copyrights>
    </MainContainer>
  );
};

export default Footer;
