import React, { Fragment } from 'react';

import Modal from 'react-modal';
import styled from 'styled-components';

import ExchangeRates from './informations/exchangeRates';
import Wallet from './informations/wallet';
import Chart from './chart';
import history from '../helpers/history';
import { fetchCurrencies } from '../services/currenciesService';
import { fetchWallet } from '../services/walletService';
import { fetchExchangeOffice } from '../services/exchangeOfficeService';
import { transaction, fetchTransactions } from '../services/transactionService';
import { breakpoints, colors } from '../styles/variables';
import { Input, Header, CancelButton, Button, Row, modalStyles, Column } from '../styles/commonStyles';

const MainContainer = styled.section`
  display: flex;
  flex-direction: column-reverse;
  justify-content: space-evenly;
  align-items: center;
  @media ${breakpoints.large} {
    flex-direction: row;
  }
`;

const MainHeader = styled.h1`
  font-size: 3rem;
  padding-top: 2rem;
  font-weight: 900;
  text-align: center;
  text-transform: uppercase;
  padding: 2rem;
  border-bottom: 0.3rem solid ${colors.blue};

`;

const ChartColumn = styled(Column)`
  align-items: center;
`;

class Exchange extends React.Component {
  constructor() {
    super();
    this.state = {
      isFetching: false,
      officeCurrencies: [],
      userCurrencies: [],
      wallet: [],
      exchangeOfficeWallet: [],
      date: null,
      userId: window.localStorage.getItem('userId'),
      amount: 1,
      currentRow: {},
      modalIsOpen: false,
      modalType: '',
      allTransactions: [],
      averagePrice: [],
      chartVisible: false
    };
    if (!window.localStorage.getItem('userToken')){
      history.push('/login');
    };
  };

  async getData() {
    this.setState({
      isFetching: true
    });
    const {items, publicationDate} = await fetchCurrencies();
    const wallet = await fetchWallet(this.state.userId);
    const exchangeOfficeWallet = await fetchExchangeOffice();
    const transactions = await fetchTransactions();
    this.setState({
      userCurrencies: items,
      officeCurrencies: items,
      date: publicationDate,
      wallet,
      exchangeOfficeWallet,
      allTransactions: transactions,
      isFetching: false
    });
  }

  async amountToData() {
    let wallet = this.state.wallet[0];
    let exchangeOfficeWallet = this.state.exchangeOfficeWallet;
    let userCurrencies = this.state.userCurrencies;
    let officeCurrencies = this.state.officeCurrencies;

    let UserAmount = userCurrencies.map((currency)=> ({
      ...currency,
      amount: wallet[currency.code]
    }));

    let OfficeAmount = officeCurrencies.map((currency)=> ({
      ...currency,
      amount: exchangeOfficeWallet.find((c)=> c.currency === currency.code).value
    }));
    this.setState({
      officeCurrencies: OfficeAmount,
      userCurrencies: UserAmount
    });
  };

  handleTransactionSell = async (body) => {
    await transaction({
      userId: this.state.userId,
      type: 'sell',
      amount: parseFloat(this.state.amount),
      currency: body.code,
      averagePrice: body.averagePrice,
      sellPrice: body.sellPrice,
      unit: body.unit
    });
    await this.getData();
    await this.amountToData();
    await this.createDataToChart();
    await this.closeModal();
  };

  handleTransactionBuy = async (body) => {
    await transaction({
      userId: this.state.userId,
      type: 'buy',
      amount: parseFloat(this.state.amount),
      currency: body.code,
      averagePrice: body.averagePrice,
      purchasePrice: body.purchasePrice,
      unit: body.unit
    });
    await this.getData();
    await this.amountToData();
    await this.createDataToChart();
    await this.closeModal();
  }

  openModal = (type, body) => {
    this.setState({
      currentRow: body,
      modalIsOpen: true,
      modalType: type,
      amount: 1
    });
  }

  closeModal = () => {
    this.setState({
      modalIsOpen: false,
      currentRow: {},
      modalType: '',
      amount: 1
    });
  }

  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    this.setState({ [name]: parseFloat(e.target.value).toFixed(2) });
  }

  handleChart = async () => {
    await this.getData();
    await this.amountToData();
    await this.createDataToChart();
    this.setState({
      chartVisible: !this.state.chartVisible
    });
  }
  createDataToChart = async () => {
    const transactions = this.state.allTransactions;
    const averagePrice = transactions.reverse().filter((e, index) => index <= 19).map(e => ({ averagePrice: e.averagePrice, currency: e.currency}));
    let data = [];
    averagePrice.map(({ currency, averagePrice}, index) => {
      return data.reverse().sort((a,b) => a.id - b.id).push({ id: (index + 1), name: currency , averagePrice: averagePrice});
    });
    this.setState({
      averagePrice: data
    });
  }

  async componentDidMount() {
    await this.getData();
    await this.amountToData();

    this.interval = setInterval(async () => {
      await this.getData();
      await this.amountToData();
    }, 20000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render () {
    const { isFetching, userCurrencies, officeCurrencies, date, modalIsOpen, currentRow, modalType, amount, averagePrice, chartVisible } = this.state;
    const value = this.state.wallet[0] && this.state.wallet[0].PLN;
      return (
        <Fragment>
          <MainContainer>
              <Column>
                <MainHeader>Buy for the lowest</MainHeader>
                <ExchangeRates
                  date={date}
                  openModal={this.openModal}
                  currencies={officeCurrencies}
                  isLoading={isFetching} />
              </Column>
              <Column>
                <MainHeader>Sell For the highest</MainHeader>
                <Wallet
                money={value}
                openModal={this.openModal}
                currencies={userCurrencies}
                isLoading={isFetching} />
              </Column>

              <Modal
                isOpen={modalIsOpen}
                onRequestClose={this.closeModal}
                className="Modal"
                overlayClassName="Overlay"
                style={modalStyles}
              >
                <Header>How much ?</Header>
                <Input
                  onChange={this.handleChange}
                  min={0.01}
                  step={0.01}
                  name="amount"
                  value={amount}
                  placeholder="amount of money..."
                  type="Number"/>
                <Row>
                  { modalType === 'sell' &&
                  <Button onClick={() => this.handleTransactionSell(currentRow)}>Confirm</Button>
                  }
                  { modalType === 'buy' &&
                  <Button onClick={() => this.handleTransactionBuy(currentRow)}>Confirm</Button>
                  }
                  <CancelButton onClick={this.closeModal}>Cancel</CancelButton>
                </Row>
              </Modal>
          </MainContainer>
          <ChartColumn>
            <Button onClick ={this.handleChart}>Chart of last 20 transactions</Button>
            {chartVisible && <Chart visible={chartVisible} data={averagePrice}/>}
          </ChartColumn>
        </Fragment>
      );
    }
  }

Modal.setAppElement('body');

export default Exchange;
