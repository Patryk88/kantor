import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { TableHeader, MainContainer } from '../styles/commonStyles';
import { colors } from '../styles/variables';


const StyledLink = styled(Link)`
  color: ${colors.green};
`;


const NotFound = () => {
  return (
    <MainContainer>
      <TableHeader>404 NOT FOUND</TableHeader>
      <TableHeader>Go back to the Site <StyledLink to="/">here</StyledLink></TableHeader>
    </MainContainer>
  );
};

export default NotFound;
