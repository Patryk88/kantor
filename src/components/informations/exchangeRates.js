import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';

import styled from 'styled-components';
import { spacings, colors } from '../../styles/variables';
import { TableContainer, TableHeader, TableButton } from '../../styles/commonStyles';

import Table from '../table';

const propTypes = {
  currencies: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      unit: PropTypes.number.isRequired,
      sellPrice: PropTypes.number.isRequired,
      purchasePrice: PropTypes.number.isRequired,
      code: PropTypes.string.isRequired,
      averagePrice: PropTypes.number.isRequired,
      amount: PropTypes.number
    })
  ),
  isLoading: PropTypes.bool,
  date: PropTypes.string
};

const Text = styled.p`
`;
const StyledDate = styled.h2`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: ${spacings.xl};
  border-bottom: .1rem solid rgba(0,0,0,0.1);
  border-left: .1rem solid rgba(0,0,0,0.1);
  border-right: .1rem solid rgba(0,0,0,0.1);
  background-color: ${colors.green};
  color: ${colors.white};
`;



const ExchangeRates = ({ currencies, isLoading, date, openModal }) => {
  const columns = [
    {
      Header: 'Currenccy',
      accessor: 'name',
      width: 110
    },
    {
      Header: 'Unit',
      accessor: 'unit',
      width: '100%'
    },
    {
      Header: 'Value',
      accessor: 'sellPrice',
      width: '100%'
    },
    {
      Header: 'Amount',
      accessor: 'amount',
      Cell: Row => parseFloat(Row.original.amount).toFixed(2),
      width: '100%'
    },
    {
      header: '',
      accessor: 'actions',
      width: '100%',
      Cell: row => {
        const diabled = row.original.amount <= 0 ? 'disabled' : '';
        return <TableButton diabled={diabled} onClick={() => {openModal('buy', row.original);}}>Buy</TableButton>;
      }
    }
  ];
  return (
    <TableContainer>
      <TableHeader>Currencies</TableHeader>
      <Table currencies={currencies} columns={columns} loading={isLoading} />
      <StyledDate><Text>Last Update:</Text><Moment format="YYYY/MM/DD - HH:MM:SS">{date}</Moment></StyledDate>
    </TableContainer>
  );
};

ExchangeRates.propTypes = propTypes;
export default ExchangeRates;
