import React from 'react';
import styled from 'styled-components';

import { spacings, colors } from '../styles/variables';

const NavContainer = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: fixed;
  top: 0;
  box-sizing: border-box;
  width: 100%;
  height: ${spacings.xxxl};
  padding: 0 ${spacings.m};
  background-color: ${colors.green};
  color: ${colors.white};
  box-shadow: 0 0 2rem 0 ${colors.shadow};
  z-index: 1;
`;

const Header = styled.h2`
  font-size: ${spacings.xl};
`;

const Navigation = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Settings = styled.p`
  font-size: ${spacings.m};
  padding-right: 2rem;
`;

const LogOut = styled.span`
  width: ${spacings.xl};
  height: ${spacings.xl};
  background-image: url(../../images/logout.svg);
  background-repeat: no-repeat;
  background-size: cover;
  cursor: pointer;
`;

const Nav = ({ loggedIn, userName, logOut }) => {
  return (
    <NavContainer>
      <Header>Kantor</Header>
      { loggedIn &&
      <Navigation>
        <Settings>Logged as {userName}</Settings>
        <LogOut onClick={logOut}/>
      </Navigation>
      }
    </NavContainer>
  );
};

export default Nav;
