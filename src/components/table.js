import React from 'react';
import ReactTable from 'react-table';

import PropTypes from 'prop-types';

import 'react-table/react-table.css';
import '../styles/table.css';


const propTypes = {
  currencies: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      unit: PropTypes.number.isRequired,
      sellPrice: PropTypes.number.isRequired,
      purchasePrice: PropTypes.number.isRequired,
      code: PropTypes.string.isRequired,
      averagePrice: PropTypes.number.isRequired
    })
  ),
  columns: PropTypes.arrayOf(
    PropTypes.shape({})
  ),
};

const Table = ({ currencies, columns, loading }) => {
  return (
    <ReactTable
      loading={loading}
      pageSize={6}
      showPagination={false}
      className="ReactTable"
      data={currencies}
      columns={columns}
      sortable={false}
      resizable={false}
    />
  );
};

Table.propTypes = propTypes;
export default Table;
