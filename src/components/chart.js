import React from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import { colors } from '../styles/variables';

const Chart = ({ data }) => (
    <LineChart width={600} height={300} data={data}>
      <XAxis dataKey="name"/>
      <YAxis dataKey="averagePrice"/>
      <CartesianGrid strokeDasharray="3 3"/>
      <Tooltip/>
      <Legend />
      <Line type="monotone" dataKey="averagePrice" stroke={colors.green}/>
    </LineChart>
);

export default Chart;
