export const colors = {
  white: '#fff',
  black: '#000',
  green: '#2ecc71',
  blue: '#3498db',
  darkerblue: '#2980b9',
  lightGray: '#ecf0f1',
  gray: '#3e3e42',
  shadow: 'rgba(15, 15, 15, 0.2)',
  purple: '#6c7ae0',
  red: '#e74c3c'
};

export const fonts = {
  roboto: 'robotoCondensedRegular',
  robotoLight: 'robotoCondensedLight',
  robotoBold: 'robotoCondensedBold'
};

export const breakpoints = {
  small: '(max-width: 767px)',
  mediumAuth: '(min-width: 540px)',
  medium: '(min-width: 768px)',
  large: '(min-width: 1280px)'
};

export const spacings = {
  xs: '.5rem',
  s: '1rem',
  m: '1.5rem',
  l: '2rem',
  xl: '3rem',
  xxl: '5rem',
  xxxl: '7.5rem'
};
