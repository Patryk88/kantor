import styled from 'styled-components';
import { colors, spacings, breakpoints } from './variables';

export const MainContainer = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 17.5rem);
  padding: ${spacings.xxl};
  @media ${breakpoints} {
    height: 100%;
  }
`;
//* tables
export const TableContainer = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: 0 0 5rem 0 ${colors.shadow};
  margin-top: 5rem;
  width: auto;
  @media ${breakpoints.medium} {
    padding: 2rem;
    width: auto;
    margin-top: 10rem;
  }
`;

export const TableHeader = styled.h2`
  display: flex;
  flex-direction: column;
  font-size: ${spacings.xl};
  font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
  text-align: center;
  padding: ${spacings.l};
`;

export const TableButton = styled.button`
  background-color: ${colors.darkerblue};
  padding: .75rem ${spacings.l};
  border: 0.2rem solid ${colors.darkerblue};
  color: white;
  font-weight: 800;
  cursor: pointer;
  :disabled {
    color: ${colors.lightGray};
    :hover {
      color: ${colors.lightGray};
    }
  }
  :hover {
    background-color: ${colors.white};
    color: ${colors.darkerblue};
  }
`;
// tables *//

export const ContainerBox = styled.section`
  display: flex;
  flex-direction: column;
  max-width: 100%;
  box-shadow: 0 0 5rem 0 ${colors.shadow};
  padding: ${spacings.xl};
  @media ${breakpoints.mediumAuth} {
    max-width: 40rem;
  }

`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const InnerContainer = styled.div``;
export const Header = styled.div`
  text-align: center;
  padding: 0.5rem;
  margin-bottom: 1.7rem;
  font-size: 2.2rem;
  border-bottom: 0.2rem solid ${colors.blue};
`;
export const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
export const InputGroup = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: ${spacings.xs};
  margin-bottom: ${spacings.xs};
`;
export const Label = styled.label`
  font-size: 2rem;
  font-weight: 600;
  padding-left: ${spacings.xs};
`;
export const Input = styled.input`
  height: 3.4rem;
  border: 0.1rem solid ${colors.blue};
  padding: ${spacings.m};
  font-size: ${spacings.l};
  color: ${colors.gray};

  :hover {
    border: 0.1rem solid ${colors.darkerblue};
  }
  :focus {
    border: 0.2rem solid ${colors.darkerblue};
    box-shadow: 0 0 2rem ${colors.show};
  }
`;

export const Button = styled.button`
  padding: 0.5rem 3rem;
  font-size: ${spacings.l};
  background-color: ${colors.blue};
  margin-top: ${spacings.l};
  border: 0.2rem solid ${colors.blue};
  width: ${({ width }) => width};
  color: ${colors.white};
  cursor: pointer;
  :focus,
  :hover {
    background-color: ${colors.white};
    color: ${colors.blue};
  }
`;

export const CancelButton = styled.button`
  padding: 0.5rem 3rem;
  font-size: ${spacings.l};
  background-color: ${colors.red};
  color: ${colors.white};
  margin-top: ${spacings.l};
  border: 0.2rem solid ${colors.red};
  width: ${({ width }) => width};
  cursor: pointer;
  :focus,
  :hover {
    background-color: ${colors.white};
    color: ${colors.red};
  }
`;

export const modalStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.55)'
  },
  content: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    border: '1px solid #ccc',
    background: '#fff',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '4px',
    outline: 'none',
    padding: '4rem'
  }
};