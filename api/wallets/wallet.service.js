const db = require('../_helpers/db');
const mongoose = require('mongoose');

const Wallet = db.Wallet;

module.exports = {
    createWallet,
    getWallets,
    getWalletById,
    updateWalletById
};

async function createWallet(walletParams) {

    const wallet = new Wallet(walletParams);

    await wallet.save();
}

async function getWallets() {

    return await Wallet.find();

}

async function getWalletById(id) {
    const objectId = mongoose.Types.ObjectId(id);
    return await Wallet.find({ userId: objectId});
}

async function updateWalletById(id, walletParams) {
    const wallet = await Wallet.findOne({ userId: id });
    Object.assign(wallet, walletParams);
    await wallet.save();
}