const express = require('express');
const router = express.Router();

const walletsService = require('./wallet.service');

router.post('/', newWallet);
router.get('/', getAllWallets);
router.get('/:id', getWallet);
router.put('/:id', updateWallet);

module.exports = router;

function newWallet(req, res, next) {
    walletsService.createWallet(req.body)
        .then(() => res.json({ message: 'Wallet Created' }))
        .catch(err => next(err));
}

function getAllWallets(req, res, next) {
    walletsService.getWallets()
        .then(wallet => wallet ? res.json(wallet) : res.sendStatus(404))
        .catch(err => next(err));
}

function getWallet(req, res, next) {
    walletsService.getWalletById(req.params.id)
        .then(wallet => wallet ? res.json(wallet) : res.sendStatus(404))
        .catch(err => next(err));
}

function updateWallet(req, res, next) {
    walletsService.updateWalletById(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}