const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  userId: {type: String, required: true},
  PLN: { type: Number, default: 0 },
  CZK: { type: Number, default: 0 },
  USD: { type: Number, default: 0 },
  EUR: { type: Number, default: 0 },
  CHF: { type: Number, default: 0 },
  RUB: { type: Number, default: 0 },
  GBP: { type: Number, default: 0 }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Wallet', schema);
