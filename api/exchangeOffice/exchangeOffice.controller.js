const express = require('express');
const router = express.Router();

const exchangeOfficeService = require('./exchangeOffice.service');

router.post('/', addCurrency);
router.get('/', getCurrencies);
router.put('/:id', updateCurrency);

module.exports = router;

function addCurrency(req, res, next) {
    exchangeOfficeService.addCurrency(req.body)
        .then(() => res.json({ message: 'Exchange Office Wallet Created' }))
        .catch(err => next(err));
}

function getCurrencies(req, res, next) {
    exchangeOfficeService.getCurrencies()
        .then(wallet => wallet ? res.json(wallet) : res.sendStatus(404))
        .catch(err => next(err));
}

function updateCurrency(req, res, next) {
    exchangeOfficeService.updateCurrency(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}