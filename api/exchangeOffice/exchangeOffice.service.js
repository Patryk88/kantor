const db = require('../_helpers/db');

const ExchangeOffice = db.ExchangeOffice;

module.exports = {
    addCurrency,
    getCurrencies,
    updateCurrency
};

async function addCurrency(walletParams) {

    const exchangeOffice = new ExchangeOffice(walletParams);

    await exchangeOffice.save();
}

async function getCurrencies() {

    return await ExchangeOffice.find();

}

async function updateCurrency(walletParams) {
    const exchangeOffice = await ExchangeOffice.findOne({ currency: walletParams.currency });
    Object.assign(exchangeOffice, walletParams);

    await exchangeOffice.save();
}