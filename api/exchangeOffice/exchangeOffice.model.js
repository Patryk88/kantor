const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  value: { type: Number, default: 0},
  currency: { type: String, unique: true }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('ExchangeOffice', schema);
