const mongoose = require('mongoose');

const config = require('../config.json');

mongoose.connect(config.connectionString, {
    useNewUrlParser: true,
    useCreateIndex: true,
});
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../users/user.model'),
    Wallet: require('../wallets/wallet.model'),
    ExchangeOffice: require('../exchangeOffice/exchangeOffice.model'),
    Transaction: require('../transactions/transaction.model')
};