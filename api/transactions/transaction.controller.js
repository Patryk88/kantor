const express = require('express');
const router = express.Router();

const transactionService = require('./transaction.service');

router.post('/', createTransaction);
router.get('/', getTransactions);

module.exports = router;

function createTransaction(req, res, next) {
  transactionService.createTransaction(req.body)
      .then(() => res.json({ message: 'Transaction Successful' }))
      .catch(err => next(err));
}

function getTransactions(req, res, next) {
  transactionService.getAllTransactions()
      .then((transactions) => transactions ? res.json(transactions) : res.sendStatus(404))
      .catch(err => next(err));
}