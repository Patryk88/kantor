const db = require('../_helpers/db');

const ExchangeOfficeService = require('../exchangeOffice/exchangeOffice.service');
const WalletsService = require('../wallets/wallet.service');

const Transaction = db.Transaction;

module.exports = {
  createTransaction,
  getAllTransactions
};

async function createTransaction(walletParams) {
    const transaction = new Transaction(walletParams);
    if( transaction.type === 'sell') {
      const wallet = await WalletsService.getWalletById(walletParams.userId);
      const exchangeOffice = await ExchangeOfficeService.getCurrencies();
      const currencyValue = exchangeOffice
        .find((c)=> c.currency === walletParams.currency).value;

      const currencyName = exchangeOffice
        .find((c)=> c.currency === walletParams.currency).currency;

      if (walletParams.amount > wallet[0][walletParams.currency]) {
        throw new Error('You do not have that much money in your wallet');
      }

      const WalletCurrency = (wallet[0][walletParams.currency] - walletParams.amount).toFixed(2);
      const WalletCurrencyPLN = (wallet[0].PLN + ((walletParams.amount / walletParams.unit) * walletParams.sellPrice)).toFixed(2);

      await WalletsService.updateWalletById(walletParams.userId, {
        [walletParams.currency]: WalletCurrency,
        PLN: WalletCurrencyPLN
      });

      const exchangeOfficeAmount = (currencyValue + walletParams.amount).toFixed(2);

      await ExchangeOfficeService.updateCurrency({
        value: exchangeOfficeAmount,
        currency: currencyName
      });
    }

    if( transaction.type === 'buy') {
      const wallet = await WalletsService.getWalletById(walletParams.userId);
      const exchangeOffice = await ExchangeOfficeService.getCurrencies();
      const currencyValue = exchangeOffice
        .find((c)=> c.currency === walletParams.currency).value;

      const currencyName = exchangeOffice
        .find((c)=> c.currency === walletParams.currency).currency;

      if (wallet[0].PLN < (walletParams.purchasePrice * walletParams.amount)) {
        throw new Error('You have not enaught money');
      } else if (walletParams.amount > currencyValue) {
        throw new Error('Not enaught amount in exchange office');
      }

      const WalletCurrency = (wallet[0][walletParams.currency] + walletParams.amount).toFixed(2);
      const WalletCurrencyPLN = (wallet[0].PLN - ((walletParams.amount / walletParams.unit) * walletParams.purchasePrice)).toFixed(2);

      await WalletsService.updateWalletById(walletParams.userId, {
        [walletParams.currency]: WalletCurrency,
        PLN: WalletCurrencyPLN
      });

      const exchangeOfficeAmount = (currencyValue - walletParams.amount).toFixed(2);

      await ExchangeOfficeService.updateCurrency({
        value: exchangeOfficeAmount,
        currency: currencyName
      });
    }

    await transaction.save();
}

async function getAllTransactions() {

  return await Transaction.find();

}