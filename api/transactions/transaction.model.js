const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  userId: {type: String, required: true},
  type: { type: String },
  currency: { type: String },
  sellPrice: { type: Number },
  purchasePrice: { type: Number },
  averagePrice: { type: Number },
  amount: { type: Number },
  unit: { type: Number },
  created: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Transaction', schema);
